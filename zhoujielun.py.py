#This piece of code crawls web data (in this case, lyrics to songs) 
#   from the web with the use of requests and json modules.

'''It demonstrates my familiarity of website development and the ability to use Python to extract useful data quickly from the web.'''
import requests
import json

url = 'https://c.y.qq.com/soso/fcgi-bin/client_search_cp'
header = {
'referer': 'https://y.qq.com/portal/search.html',
'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36'
}

for i in range (1,6):
    param = {
    'ct':' 24',
'qqmusic_ver': '1298',
'new_json':' 1',
'remoteplace': 'txt.yqq.song',
'searchid': '68018834503304261',
't':' 0',
'aggr': '1',
'cr': '1',
'catZhida':' 1',
'lossless': '0',
'flag_qc': '0',
'p': str(i),
'n':' 10',
'w': '周杰伦',
'g_tk': '5381',
'loginUin': '0',
'hostUin': '0',
'format': 'json',
'inCharset': 'utf8',
'outCharset': 'utf-8',
'notice': '0',
'platform': 'yqq.json',
'needNewCode': '0',
    }
    res = requests.get (url, params = param)
    json_lyric = res.json()
    lyrics = json_lyric ['data']['lyric']['list']
    for lyric in lyrics:
        print (lyric['content'])
    


print ("Program termina ted.")